import React, {Component} from 'react';
import './App.css';
import Home from '../components/Home/Home.jsx';
import AboutUs from '../components/AboutUs/AboutUs';
import ContactUs from '../components/ContactUs/ContactUs.jsx';
import {Link ,BrowserRouter as Router,Route} from 'react-router-dom';


class  App extends Component
{
  state={
    todolist:[]
  }
  render(){
  return (
   <Router > 
     <div className="App" >
    
     <ul> <Link to="/">Home      </Link>
      
       
      <Link to="/AboutUs"> About Us          </Link>
       
      
      <Link to="/ContactUs">Contact US        </Link>
      </ul>
     
     
       </div>
       <Route exact path="/" component={Home}></Route>
  <Route path="/AboutUs" component={AboutUs}></Route>
  <Route path="/ContactUs" component={ContactUs}></Route>
  {/* <Switch>
  <Route path="/">
            <Home />
          </Route>
          <Route path="/About">
            <AboutUs />
          </Route>
          <Route path="/ConatactUs">
            <ContactUs />
          </Route>
          
        </Switch> */}
 
      
      
      
   </Router>
   
  );
}
}

export default App;
