import React from 'react';
import {TextField} from '@material-ui/core';

const ContactUs= () =>{
    return(
        <div>
        <TextField
          label="Enter Question Here"
          colour='secondary'
          placeholder="Placeholder"
          helperText="Full width!"
          fullWidth
          margin="normal"
          variant="filled"
        />
        </div>
    );
    
};
export default ContactUs;