import React, { useState } from 'react';
import Todo from '../Todo/Todo';
import TodoList from '../TodoList/TodoList';

const Home= (props) =>{
    const [todos,setTodos]=useState([]);
    const [curTodo,setCurTodo]=useState("");
  
    const addTodo =(e)=>{
          e.preventDefault();
         if(curTodo){
     setTodos([...todos,curTodo]);
     setCurTodo("");
         }
         else{
              alert("Please Dont Add Empty  character");
         }
}
   
    const todoEditing=(e)=>
    {
         setCurTodo(e.target.value);
    }
    return(
        <div>
                   <Todo todoEditing={todoEditing} addTodo={addTodo} curTodo={curTodo} />
     <div >
          <TodoList todos={todos}/>
              
     </div>
  </div>
  );
    
};
export default Home;